import ast
import copy
import sys
import json

import numpy as np

# D = [50, 70, 30, ]
# O = [20, 40, 90]

D = [20,40,38 ]
O = [27,27,36]
cost_matrix = np.array(
    [[1,2,7],
     [3,9,11],
     [2,7,8], ]
)


# D = [32, 19, 27]
# O = [20, 40, 40]

# D=[30,30,30]
# O=[30,30,30]

# cost_matrix = np.array([[3, 5, 7, ],
#                         [12, 10, 9, ],
#                         [13, 3, 9, ], ])

# D = [50, 40, 60]
# O = [20, 95, 35]
# cost_matrix = np.array([[6, 4, 1],
#                         [3, 8, 7],
#                         [4, 4, 2], ])

# D = [20, 30, 10, 40]
# O = [10, 15, 30, 10, 35]
# cost_matrix = np.array([[5,3,1,2,2],
#                         [2,1,1,1,1],
#                         [1,1,2,5,2],
#                         [5,4,3,1,6],])

# O = [40, 60, 80, 60]
# D = [60, 80, 100]
# cost_matrix = np.array([[1, 2, 3, 4],
#                         [4, 3, 2, 0],
#                         [0, 2, 2, 1], ])


# print(koszt)


###
# WIERZCHOLEK NW
###
def create_matrix(D, O):
    sumD = np.sum(D)
    sumO = np.sum(O)
    if sumD != sumO:
        diff = sumD - sumO
        if diff > 0:
            O = np.pad(O, (0, 1), mode='constant', constant_values=diff)
        else:
            D = np.pad(D, (0, 1), mode='constant', constant_values=-diff)

    lengthY = len(D)
    lengthX = len(O)

    matrix = np.zeros((lengthY, lengthX))

    x = 0
    y = 0

    while x < lengthX and y < lengthY:
        if D[y] > O[x]:
            matrix[y][x] = O[x]
            D[y] = D[y] - O[x]
            x += 1
        elif D[y] < O[x]:
            matrix[y][x] = D[y]
            O[x] = O[x] - D[y]
            y += 1
        else:
            matrix[y][x] = D[y]
            x += 1
            y += 1
    # print(lengthX,lengthY)
    # print(matrix.shape)
    # print(matrix)
    return matrix


def find_loop(x_base, y_base, matrix, visited=[]):
    def check_if_in_line(p1, p2):
        x1 = p1[0]
        y1 = p1[1]
        x2 = p2[0]
        y2 = p2[1]
        return x1 == x2 or y1 == y2

    visited = copy.deepcopy(visited)

    if (x_base, y_base) in visited:
        return None

    try:
        prev_node = visited[0] == visited[-1]
    except IndexError:
        prev_node = 0

    try:
        a = check_if_in_line((x_base, y_base), visited[-2])
    except IndexError:
        pass

    output = []
    lengthY, lengthX = matrix.shape
    if len(visited) < 2 or not check_if_in_line((x_base, y_base), visited[-2]):
        visited.append((x_base, y_base))
        for x in range(x_base)[::-1]:
            if not prev_node and (x, y_base) == visited[0]:
                return visited
            if matrix[y_base][x]:
                tmp = find_loop(x, y_base, matrix, visited)
                if tmp:
                    output.append(tmp)
                    break
        for x in range(x_base + 1, lengthX):
            if not prev_node and (x, y_base) == visited[0]:
                return visited
            if matrix[y_base][x]:
                tmp = find_loop(x, y_base, matrix, visited)
                if tmp:
                    output.append(tmp)
                    break
        for y in range(y_base)[::-1]:
            if not prev_node and (x_base, y) == visited[0]:
                return visited
            if matrix[y][x_base]:
                tmp = find_loop(x_base, y, matrix, visited)
                if tmp:
                    output.append(tmp)
                    break
        for y in range(y_base + 1, lengthY):
            if not prev_node and (x_base, y) == visited[0]:
                return visited
            if matrix[y][x_base]:
                tmp = find_loop(x_base, y, matrix, visited)
                if tmp:
                    output.append(tmp)
                    break
        if output:
            return output[0]
        else:
            return None


def reverse_order_of_point(list_of_points):
    return tuple(t[::-1] for t in list_of_points)


def find_opportunity_cost(path, cost_matrix):
    # print(path)
    # print(cost_matrix)
    path = reverse_order_of_point(path)  # reverse (x,y) to (y,x)
    positives = [cost_matrix[n] for n in path[0::2]]
    negatives = [cost_matrix[n] for n in path[1::2]]
    return np.sum(positives) - np.sum(negatives)


if __name__ == '__main__':
    D = np.array(json.loads(sys.argv[1]))
    O = np.array(json.loads(sys.argv[2]))
    cost_matrix = np.array(ast.literal_eval(sys.argv[3]))

    # print(D,O,cost_matrix, sep='\n')


    matrix = create_matrix(D, O)
    print(matrix)
    # print(np.transpose(matrix))

    list_of_empty_cells = np.where(matrix == 0)
    list_of_empty_cells = tuple(zip(*list_of_empty_cells[::-1]))  # zamien na format (x,y) z [y][x]
    # print(list_of_empty_cells,end='\n\n')
    paths = [find_loop(*y, matrix) for y in list_of_empty_cells]
    # tmp = find_loop(*(2, 0), matrix)
    # print(*paths, sep='\n')
    opportunity_costs = [find_opportunity_cost(path, cost_matrix) for path in paths]

    while np.amin(opportunity_costs) < 0:
        # print(opportunity_costs)
        min_idx = np.argmin(opportunity_costs)
        chosen = paths[min_idx]
        # print(chosen)
        values = np.array([matrix[point] for point in reverse_order_of_point(chosen)])
        # print(values)
        sub_values = values[1::2]  # pick only those from which we can subtract
        delta_idx = 1 + 2 * np.where(sub_values != 0, sub_values,
                                     np.inf).argmin()  # find lowest positive number by changing non positive numbers into infinity
        delta = values[delta_idx]
        for sub in reverse_order_of_point(chosen)[1::2]:
            matrix[sub] -= delta
        for add in reverse_order_of_point(chosen)[0::2]:
            matrix[add] += delta

        # print(np.transpose(matrix))

        list_of_empty_cells = np.where(matrix == 0)
        list_of_empty_cells = tuple(zip(*list_of_empty_cells[::-1]))  # zamien na format (x,y) z [y][x]
        paths = [find_loop(*y, matrix) for y in list_of_empty_cells]
        # print(*paths, sep='\n')
        opportunity_costs = [find_opportunity_cost(path, cost_matrix) for path in paths]

        print(matrix)
    total_cost = np.sum(np.multiply(matrix,cost_matrix))
    print(total_cost)
    matrix_str = np.array_repr(matrix).replace('\n','')
    print(matrix_str)